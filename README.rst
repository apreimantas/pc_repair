========
Overview
========

This is a simple OpenERP 7.0 compatible addon for Computer Repairs shop.

Using the module you can track simple computer repair tasks. It is integrated
with warehouse management so adding products in page "Charges" will be reflected
on stock levels.

The module was developed some time ago and haven't seen no action since, so
you are free to fork it and change it according to your needs.

Screenshots
========

.. image:: https://bitbucket.org/apreimantas/pc_repair/raw/1b3905b9b89a9fc209a3446346f93ed7d2d33177/static/src/img/repair_order_form_view.png

.. image:: https://bitbucket.org/apreimantas/pc_repair/raw/dd38b0fb01367e16c11081d3b0711b8006ee4715/static/src/img/repair_order_list_view.png
