# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012 Andrius Preimantas (<http://administravimas.lt>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp import tools
from tools.translate import _

class report_repair_order(osv.osv):
    _name = "report.repair.order"
    _description = "Order reporting"
    _auto = False
    _columns = {
        'name': fields.char('Order no.', size=128, readonly=True),
        'day': fields.char('Day', size=128, readonly=True),
        'year': fields.char('Year', size=64, required=False, readonly=True),
        'month':fields.selection([('01', 'January'), ('02', 'February'), ('03', 'March'), ('04', 'April'), ('05', 'May'), ('06', 'June'), ('07', 'July'), ('08', 'August'), ('09', 'September'), ('10', 'October'), ('11', 'November'), ('12', 'December')], 'Month', readonly=True),
        'weekday': fields.selection([(1, _('Monday')), (2, _('Tuesday')),
            (3, _('Wednesday')), (4, _('Thursday')), (5, _('Friday')), (6, _('Saturday')), (7, _('Sunday'))], readonly=True),
        'partner_id': fields.many2one('res.partner', 'Contact', readonly=True),
        'backup_location_id': fields.many2one('repair.order.backup_location', _('Backup Location'), readonly=True),
        'state': fields.selection([('draft', _('Draft')), ('new', _('New')), \
            ('in_progress', _('In Progress')), ('pending', _('Pending')), \
            ('completed', _('Completed')), ('paid', _('Paid')), ('canceled', _('Canceled'))], _('State'), readonly=True)
    }
    _order = 'name desc, day desc'

    def init(self, cr):
        tools.sql.drop_view_if_exists(cr, 'report_repair_order')
        cr.execute("""
            CREATE view report_repair_order as
              SELECT
                    t.id as id,
                    to_char(create_date, 'YYYY') as year,
                    to_char(create_date, 'MM') as month,
                    to_char(create_date, 'YYYY-MM-DD') as day,
                    extract(dow from create_date) as weekday,
                    t.backup_location_id,
                    t.name as name,
                    t.partner_id,
                    t.state
             FROM repair_order t
                GROUP BY
                    t.id,
                    year,
                    month,
                    day,
                    name,
                    t.partner_id,
                    t.state,
                    weekday
        """)

report_repair_order()

