# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012 Andrius Preimantas (<http://administravimas.lt>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields
from tools.translate import _
import time
import decimal_precision as dp


class repair_order(osv.osv):
    _name = 'repair.order'
    _description = 'Repair Order'

    _order = "name desc"
    #add OpenChatter feature
    _inherit = ['ir.needaction_mixin', 'mail.thread']
    _track = {
        'state': {
            'pc_repair.mt_repair_completed': lambda self, cr, uid, obj, ctx = None: obj['state'] == 'completed',
            'pc_repair.mt_repair_validated': lambda self, cr, uid, obj, ctx = None: obj['state'] == 'new',
            },
        }


    _columns = {
        'partner_id': fields.many2one('res.partner', _('Client'), required=True),
        'location_id': fields.many2one('stock.location', _('Location'), help=_('Location where customer\'s item related with this order is located')),
        'line_ids': fields.one2many('repair.order.line', 'order_id', _('Repair Lines')),

        'name': fields.char(_('Repair number'), size=64, readonly=True),
        'problem_description': fields.text(_('Problem Description')),

        'problem_resolution': fields.text(_('Resolution Summary')),

        'date_create': fields.datetime(_('Create Date'), help=_('Date when this order '\
            'was created'), readonly=True),
        'date_collected': fields.datetime(_('Collect Date')),
        'date_completed': fields.datetime(_('Date Completed'), readonly=True),

        'comment': fields.text(_('Inner Comment')),

        #Additional info
        'estimated_price': fields.float(_('Estimated Price')),
        'password': fields.char(_('Password'), size=128, required=True),
        'important_data': fields.selection([('yes', _('Yes')), \
            ('no', _('No')), ('check', _('Check'))],
            string=_('Important Data'), required=True),
        'outlook': fields.selection([('yes', _('Yes')), \
            ('no', _('No')), ('check', _('Check'))], string=_('Outlook'),
            required=True),
        'serial_number': fields.char(_('Serial Number'), size=128),

        #Customers' Items 
        'laptop_adapter': fields.boolean(_('Laptop Power Adapter')),
        'laptop_bag': fields.boolean(_('Laptop Bag')),
        'power_cord': fields.boolean(_('Power Cord')),
        'printer': fields.boolean(_('Printer')),
        'system_discs': fields.boolean(_('System Discs')),
        'display': fields.boolean(_('Display')),
        'modem': fields.boolean(_('Modem')),
        'router': fields.boolean(_('Router')),
        'flash_drive': fields.boolean(_('Flash Drive')),
        'external_hdd': fields.boolean(_('External HDD or CD')),
        'mouse': fields.boolean(_('Mouse')),
        'keyboard': fields.boolean(_('Keyboard')),
        'air_card': fields.boolean(_('Air Card')),
        'other': fields.char(_('Other'), size=128),

        # Testing
        'hdd1': fields.char(_('HDD1 (health %)'), size=32),
        'hdd1_pass': fields.selection([('pass', _('Pass')), ('fail', _('Fail')), \
            ('skip', _('Skip'))], 'HDD1 '),
        'hdd2': fields.char(_('HDD2 (health %)'), size=32),
        'hdd2_pass': fields.selection([('pass', _('Pass')), ('fail', _('Fail')), \
            ('skip', _('Skip'))]),
        'ram': fields.char(_('RAM (passes %)'), size=32),
        'ram_pass': fields.selection([('pass', _('Pass')), ('fail', _('Fail')), \
            ('skip', _('Skip'))]),
        'overheating': fields.selection([('yes', _('Yes')), ('no', _('No')), \
            ('cleaned', _('Cleaned'))], 'Overheating'),
        'test_notes': fields.text(_('Notes')),

        #Other
        'backup_location_id': fields.many2one('repair.order.backup_location', _('Backup Location')),
        'delay_reason': fields.selection([('waiting_client', _('Client Acknowledgment Needed')),
            ('warranty_service', _('In Warranty Service')),
            ('waiting_parts', _('Waiting for Parts'))], _('Delay Reason')),

        'priority': fields.selection([('low', _('Low')), ('medium', _('Medium')),
            ('high', _('High'))], string=_('Priority')),

        'state': fields.selection([('draft', _('Draft')), ('new', _('New')), \
            ('in_progress', _('In Progress')), ('pending', _('Pending')), \
            ('completed', _('Completed')), ('paid', _('Paid')), ('canceled', _('Canceled'))], _('State'),
            track_visibility='always'),

        }

    _defaults = {
        'date_create': lambda *a: time.strftime('%Y-%m-%d %H:%M:%S'),
        'state': 'draft',
        'priority': 'medium',
        }


    def write(self, cr, uid, ids, values, context=None):
        if 'delay_reason' in values:
            order_obj = self.browse(cr, uid, ids)[0]
            if values['delay_reason']:
                values['state'] = 'pending'
            elif order_obj.state == 'pending':
                    values['state'] = 'in_progress'

        return super(repair_order, self).write(cr, uid, ids, values, context=context)


    def name_get(self, cr, uid, ids, context=None):
        res = []
        for r in self.browse(cr, uid, ids, context=context):
            name = r.name or str(r.id) + ' (draft)'
            client = r.partner_id and r.partner_id.name or 'no partner'
            res.append((r.id, '%s (%s)' % (name, client)))
        return res

    def default_get(self, cr, uid, fields, context=None):
        to_ret = super(repair_order, self).default_get(cr, uid, fields, context=context)
        to_ret['location_id'] = context.get('location_id', False)
        return to_ret

    def action_draft(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'draft'})
#        for oid in ids:
#            self.log(cr, uid, oid, _('Order created (object id=%s)' % oid))
        return True

    def action_confirm(self, cr, uid, ids, context=None):
        if not context:
            context = {}
        for obj in self.browse(cr, uid, ids):
            #Order might have number if it was reset from canceled. We keep that number
            new_id = obj.name
            if not new_id:
                ir_seq = self.pool.get('ir.sequence')
                new_id = ir_seq.next_by_code(cr, uid, 'default_repair_ordering')
                if not new_id:
                    raise osv.except_osv(_('Error!'), _('Please define sequence of type \'default_repair_ordering\''))
            self.write(cr, uid, ids, {'name': new_id, 'state': 'new'})
#            self.log(cr, uid, obj.id, _('Order state changed to New (%s)' % new_id))
        return True

    def action_in_progress(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'in_progress', 'delay_reason': ''})
        return True

    def action_completed(self, cr, uid, ids, context=None):
        stock_move_pool = self.pool.get('stock.move')
        self.write(cr, uid, ids, {'state': 'completed'})
        for oid in ids:
            obj = self.browse(cr, uid, oid, context=context)
            for line in obj.line_ids:
                if line.stock_move_id:
                    stock_move_pool.action_done(cr, uid, [line.stock_move_id.id], context=context)
#            self.log(cr, uid, oid, _('Order state changed to Finished (%s)' % self.browse(cr, uid, oid).name))
        self.write(cr, uid, ids, {'date_completed': time.strftime('%Y-%m-%d %H:%M:%S')})
        return True


    def action_cancel(self, cr, uid, ids, context=None):
        if not ids: return False
        line_pool = self.pool.get('repair.order.line')
        for obj in self.browse(cr, uid, ids, context=context):
            line_pool.cancel_moves(cr, uid, obj.line_ids, context=context)
            self.write(cr, uid, obj.id, {'state': 'canceled'}, context=context)
        return True


    def action_to_draft(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'draft', 'delay_reason': ''})
        return True


class repair_order_line(osv.osv):
    _name = 'repair.order.line'


    def _line_subtotal(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids):
            res[line.id] = float(line.price) * float(line.quantity)
        return res

    _columns = {
        'order_id': fields.many2one('repair.order', _('Order'), required=True,
            ondelete="cascade"),
        'title': fields.char(_('Title'), size=256),

        'product_id': fields.many2one('product.product', _('Product')),
        'stock_move_id': fields.many2one('stock.move', _('Stock Move')),
        'quantity': fields.float(_('Quantity')),
        'price': fields.float(_('Price')),

        'price_subtotal': fields.function(_line_subtotal, string=_('Subtotal')),
        }

    _defaults = {
        'quantity': 1.00,
        }

    def _check_order_id(self, cr, uid, ids, context=None):
        for obj in self.browse(cr, uid, ids, context=context):
            if not obj.order_id:
                return False
        return True


    def _check_quantity(self, cr, uid, ids, context=None):
        for obj in self.browse(cr, uid, ids, context=context):
            if not obj.quantity or obj.quantity <= 0.0:
                return False
        return True


    _constraints = [
        (_check_order_id, _('Order line must be associated with order object.'), ['order_id']),
        (_check_quantity, _('Quantity must be > 0'), ['quantity']),
    ]


    def _get_move_details(self, cr, uid, values, context):
        prod_obj = self.pool.get('product.product').browse(cr, uid, values.get('product_id', False))
        ord_id = values.get('order_id', None)
        order_obj = self.pool.get('repair.order').browse(cr, uid, ord_id)
        company_id = self.pool.get('res.users')._get_company(cr, uid)
        return {
                'company_id': company_id,
                'date': time.strftime('%Y-%m-%d %H:%M:%S'),
                'date_expected': time.strftime('%Y-%m-%d %H:%M:%S'),
                'location_dest_id': context.get('location_dest_id', False),
                'location_id': context.get('location_id', False),
                'name': values.get('title', False) or '/',
                'origin': 'Repair order: ' + order_obj.name,
                'product_id': prod_obj.id,
                'product_qty': values.get('quantity', False), #leave it as is; if no quantity gets passed let's get normal exception
                'product_uom': prod_obj.uom_id.id,
                }


    def onchange_product_id(self, cr, uid, ids, product_id):
        product_obj = self.pool.get('product.product').browse(cr, uid, product_id)
        to_ret = {}
        return {'value': {
                'title': product_obj.name,
                'price': product_obj.list_price,
            }}


    def onchange_price(self, cr, uid, ids, quantity, price, context=None):
        return {'value':
            {'price_subtotal': float(price) * float(quantity or 0.0)}
            }


    def action_completed(self, cr, uid, ids, context=None):
        pass


    def create(self, cr, uid, values, context=None):
        prod_id = values.get('product_id', False)
        if prod_id:
            move_pool = self.pool.get('stock.move')
            prod_obj = self.pool.get('product.product').browse(cr, uid, prod_id)
            move_vals = self._get_move_details(cr, uid, values, context)
            move_id = move_pool.create(cr, uid, move_vals)
            move_pool.action_confirm(cr, uid, [move_id], context=context)
            values['stock_move_id'] = move_id
        return super(repair_order_line, self).create(cr, uid, values, context=context)


    def cancel_moves(self, cr, uid, line_ids, context=None):
        move_ids = []
        for obj in self.browse(cr, uid, line_ids, context=context):
            if obj.stock_move_id:
                move_ids.append(obj.stock_move_id.id)
        self.pool.get('stock.move').action_cancel(cr, uid, move_ids, context=context)
        return True

    def unlink(self, cr, uid, ids, context=None):
        self.cancel_moves(cr, uid, ids, context=context)
        return super(repair_order_line, self).unlink(cr, uid, ids, context=context)


class repair_order_backup_location(osv.osv):
    _name = 'repair.order.backup_location'

    _columns = {
        'name': fields.char(_('Location Title'), required=True),
        'active': fields.boolean(_('Active')),
        'description': fields.text(_('Description')),
        }

    _defaults = {
        'active': True,
        }
